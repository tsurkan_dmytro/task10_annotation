package epam.task.anotatoons.anotationdeclare;

public  @interface PoverSelektor{
    int value1() default 1;
    String value2() default "";
    String value3() default "xyz";
}